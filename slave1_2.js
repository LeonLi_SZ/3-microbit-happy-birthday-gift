// 4801 for both Slaves
control.onEvent(4800, 4801, function () {
    music._playDefaultBackground(music.builtInPlayableMelody(Melodies.Birthday), music.PlaybackMode.InBackground)
})
input.onButtonPressed(Button.A, function () {
    isSlave1 = true
    music.play(music.tonePlayable(247, music.beat(BeatFraction.Whole)), music.PlaybackMode.UntilDone)
})
// 4803 only for Slave1
control.onEvent(4800, 4803, function () {
    if (isSlave1) {
        basic.showString("Happy Birthday!")
        basic.showIcon(IconNames.Heart)
    }
})
input.onButtonPressed(Button.B, function () {
    isSlave1 = false
    music.play(music.tonePlayable(440, music.beat(BeatFraction.Whole)), music.PlaybackMode.UntilDone)
})
// 4805 only for Slave2
control.onEvent(4800, 4805, function () {
    if (!(isSlave1)) {
        basic.showString("Happy Birthday!")
        basic.showIcon(IconNames.Heart)
    }
})
let isSlave1 = false
basic.showString("S")
basic.pause(2000)
radio.setGroup(18)
isSlave1 = true
// auto LED backlight
loops.everyInterval(5000, function () {
    led.setBrightness(Math.constrain(pins.map(
    input.lightLevel(),
    0,
    200,
    50,
    255
    ), 50, 255))
})

input.onButtonPressed(Button.A, function () {
    dly_ms = 1450
    music.play(music.tonePlayable(220, music.beat(BeatFraction.Whole)), music.PlaybackMode.UntilDone)
})
control.onEvent(4800, 4802, function () {
    basic.pause(dly_ms)
    // inform Slave1
    radio.raiseEvent(
    4800,
    4803
    )
    // no need 4804 anymore, just 1 more delay of dly_ms, the sent 4805 via RF
    basic.pause(dly_ms)
    // inform Slave2
    radio.raiseEvent(
    4800,
    4805
    )
})
input.onButtonPressed(Button.B, function () {
    dly_ms = 1650
    music.play(music.tonePlayable(494, music.beat(BeatFraction.Whole)), music.PlaybackMode.UntilDone)
})
input.onLogoEvent(TouchButtonEvent.Pressed, function () {
    dly_ms = 1550
    music.play(music.tonePlayable(330, music.beat(BeatFraction.Whole)), music.PlaybackMode.UntilDone)
})
let dly_ms = 0
basic.showString("M")
basic.pause(2000)
radio.setGroup(18)
dly_ms = 1550
loops.everyInterval(22000, function () {
    // 4801 to inform 2 Slaves play Happy Birthday melody
    radio.raiseEvent(
    4800,
    4801
    )
    // fine-tune value
    control.waitMicros(10)
    // Master starts play after 4801 issued
    music._playDefaultBackground(music.builtInPlayableMelody(Melodies.Birthday), music.PlaybackMode.InBackground)
    // 4802/4803 to inform Slave1 show "Happy Birthday!"
    control.raiseEvent(
    4800,
    4802
    )
    // Master starts to show immediately
    basic.showString("Happy Birthday!")
    basic.showIcon(IconNames.Heart)
})
// auto LED backlight
loops.everyInterval(5000, function () {
    led.setBrightness(Math.constrain(pins.map(
    input.lightLevel(),
    0,
    200,
    50,
    255
    ), 50, 255))
})
